﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Tetris_LAN_20193995
{
    public partial class Form1 : Form
    {
        int score = 0;
        int mainWidth = 15;
        int mainHeight = 20;
        int[,] mainDotArray;
        int dotSize = 20;
        int currentX;
        int currentY;
        Bitmap workingBitmap;
        Graphics workingGraphics;
        Bitmap mainBitmap;
        Graphics mainGraphics;
        Shape cShape;
        Timer timer = new Timer();

        public int Score { get => score; set => score = value; }
        public int MainWidth { get => mainWidth; set => mainWidth = value; }
        public int MainHeight { get => mainHeight; set => mainHeight = value; }
        public int[,] MainDotArray { get => mainDotArray; set => mainDotArray = value; }
        public int DotSize { get => dotSize; set => dotSize = value; }
        public int CurrentX { get => currentX; set => currentX = value; }
        public int CurrentY { get => currentY; set => currentY = value; }
        public Bitmap WorkingBitmap { get => workingBitmap; set => workingBitmap = value; }
        public Graphics WorkingGraphics { get => workingGraphics; set => workingGraphics = value; }
        public Bitmap MainBitmap { get => mainBitmap; set => mainBitmap = value; }
        public Graphics MainGraphics { get => mainGraphics; set => mainGraphics = value; }
        public Shape CShape { get => cShape; set => cShape = value; }
        public Timer Timer { get => timer; set => timer = value; }

        public Form1()
        {
            InitializeComponent();
            this.BackColor = Color.Green;
            label1.Text = "Score: " + Score;
            label2.Text = "Level: " + Score / 10;
            LoadMain();
            CShape = GetRandomShapeInMiddle();
            Timer.Tick += Timer_Tick;
            Timer.Interval = 400;
            Timer.Start();
            this.KeyDown += GetKeyMoves;
        }


        private void LoadMain()
        {
            Game_console.Width = MainWidth * DotSize;
            Game_console.Height = MainHeight * DotSize;
            MainBitmap = new Bitmap(Game_console.Width, Game_console.Height);
            MainGraphics = Graphics.FromImage(MainBitmap);
            MainGraphics.FillRectangle(Brushes.LightGreen, 0, 0, MainBitmap.Width, MainBitmap.Height);
            Game_console.Image = MainBitmap;
            MainDotArray = new int[MainWidth, MainHeight];
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            var isMoveSuccess = MoveShapeIfPossible(moveDown: 1);

            if (!isMoveSuccess)
            {
                MainBitmap = new Bitmap(WorkingBitmap);
                InsertShapeInMainDotArray();
                CShape = GetRandomShapeInMiddle();
                ClearFilledRowsAndUpdateScore();
            }
        }
        private bool MoveShapeIfPossible(int moveDown = 0, int moveSide = 0)
        {
            var newX = CurrentX + moveSide;
            var newY = CurrentY + moveDown;
            if (newX < 0 || newX + CShape.Width > MainWidth
                || newY + CShape.Height > MainHeight)
                return false;
            for (int i = 0; i < CShape.Width; i++)
            {
                for (int j = 0; j < CShape.Height; j++)
                {
                    if (newY + j > 0 && MainDotArray[newX + i, newY + j] == 1 && CShape.Dots[j, i] == 1)
                        return false;
                }
            }
            CurrentX = newX;
            CurrentY = newY;
            DrawShape();

            return true;
        }

        private void DrawShape()
        {
            WorkingBitmap = new Bitmap(MainBitmap);
            WorkingGraphics = Graphics.FromImage(WorkingBitmap);

            for (int i = 0; i < CShape.Width; i++)
            {
                for (int j = 0; j < CShape.Height; j++)
                {
                    if (CShape.Dots[j, i] == 1)
                        WorkingGraphics.FillRectangle(Brushes.Black, (CurrentX + i) * DotSize, (CurrentY + j) * DotSize, DotSize, DotSize);
                }
            }

            Game_console.Image = WorkingBitmap;
        }

        private Shape GetRandomShapeInMiddle()
        {
            var shape = ShapeTypes.GetRandomShape();
            CurrentX = 7;
            CurrentY = -shape.Height;

            return shape;
        }


        private void InsertShapeInMainDotArray()
        {
            for (int i = 0; i < CShape.Width; i++)
            {
                for (int j = 0; j < CShape.Height; j++)
                {
                    if (CShape.Dots[j, i] == 1)
                    {
                        CheckIfGameOver();
                        try
                        {
                            MainDotArray[CurrentX + i, CurrentY + j] = 1;
                        }
                        catch (System.IndexOutOfRangeException)
                        {
                            Application.Exit();
                        }
                    }
                }
            }
        }
        private void GetKeyMoves(object sender, KeyEventArgs e)
        {
            var verticalMove = 0;
            var horizontalMove = 0;

            switch (e.KeyCode)
            {
                case Keys.Left:
                    verticalMove--;
                    break;

                case Keys.Right:
                    verticalMove++;
                    break;

                case Keys.Down:
                    horizontalMove++;
                    break;

                case Keys.Up:
                    CShape.turnShape();
                    break;

                default:
                    return;
            }

            var isMoveSuccess = MoveShapeIfPossible(horizontalMove, verticalMove);

            if (!isMoveSuccess && e.KeyCode == Keys.Up)
                CShape.rollbackShape();
        }

        public void ClearFilledRowsAndUpdateScore()
        {
            for (int i = 0; i < MainHeight; i++)
            {
                int j;
                for (j = MainWidth - 1; j >= 0; j--)
                {
                    if (MainDotArray[j, i] == 0)
                        break;
                }

                if (j == -1)
                {
                    Score=Score+Score/10+5;
                    label1.Text = "Score: " + Score;
                    label2.Text = "Level: " + Score / 10;
                    Timer.Interval -= 10;

                    for (j = 0; j < MainWidth; j++)
                    {
                        for (int k = i; k > 0; k--)
                        {
                            MainDotArray[j, k] = MainDotArray[j, k - 1];
                        }

                        MainDotArray[j, 0] = 0;
                    }
                }
            }
            for (int i = 0; i < MainWidth; i++)
            {
                for (int j = 0; j < MainHeight; j++)
                {
                    MainGraphics = Graphics.FromImage(MainBitmap);
                    MainGraphics.FillRectangle(
                        MainDotArray[i, j] == 1 ? Brushes.Black : Brushes.LightGreen,
                        i * DotSize, j * DotSize, DotSize, DotSize
                        );
                }
            }

            Game_console.Image = MainBitmap;
        }
        private void CheckIfGameOver()
        {
            if (CurrentY < 0)
            {
                Timer.Stop();
                MessageBox.Show("Game Over");
                Application.Exit();
            }
        }

    }

}
